---
sidebarDepth: 2
---

# Team Handbook

The Meltano team handbook is the central repository for how the team functions and builds the product.

As Meltano was incubated in [GitLab](https://about.gitlab.com/) and later [spun out](https://meltano.com/blog/2021/06/30/meltano-spins-out-of-gitlab-raises-seed-round),
many of our policies and processes are inspired by (if not outright copied from) theirs.
If a topic isn't covered here yet, the corresponding part of GitLab's handbook can generally be assumed to apply,
but we should explicitly move things over to our own handbook so that we can adapt things to be more appropriate for our size and unique culture.

## Subresources

- [Engineering Handbook](/handbook/engineering/)
- [Marketing Handbook](/handbook/marketing/)
- [Product Handbook](/handbook/product/)
- [Tech Stack](/handbook/tech-stack/)
- [Data Learning Resources](/handbook/resources/)

## Benefits

* Unlimited PTO
* Professional Development Support and Budget
* Remote Working
* Reimbursable coworking fees and external office space
* Budget for Office equipment and workspace supplies
* Team Offsites
* Access to world class founders, investors and mentors

### Family and Friends Days

Inspired by GitLab's [Family and Friends Day](https://about.gitlab.com/company/family-and-friends-day/), Meltano has made Family and Friends Day a regular part of the culture. 
On these days, we will close the doors to the Meltano virtual office, reschedule all meetings, and have a publicly visible shutdown.

Team members can share about their Family and Friends Day in the #family-and-friends-day Slack channel after the event, or publicly on social media such as Twitter, LinkedIn, or wherever they're most comfortable using the hashtag #FamilyFriends1st. 
Sharing is optional. 
Taking the day off is strongly encouraged if your role allows it.

#### Upcoming Family and Friends Days

* 2021-08-20 - The first Meltano Family & Friends Day
* 2021-09-17
* 2021-10-15
* 2021-11-24

These dates are tracked in the [Team Meetings Calendar](/handbook/#calendars). 
Meltano employees should mark these days off using [PTO by Roots](/handbook/tech-stack/#pto-by-roots).

In line with our Paid Time Off policy, we encourage Meltano Team Members to continue to take additional days off, as needed. 
Family and Friends Day is a reminder to do this.

## Customer Support

### Email

Emails to `hello@meltano.com` forward to Zendesk so they can be triaged, assigned, and managed.

#### Responsible Disclosure of Security Vulnerabilities

Emails to `security@meltano.com` also forward to Zendesk, and are automatically assigned to the Security group, which includes all current team members.
As documented in our [Responsible Disclosure Policy](/docs/responsible-disclosure.md), we will acknowledge receipt of a vulnerability report the next business day and strive to send the reporter regular updates about our progress.

### Intercom Live Chat

The [Meltano.com website](https://www.meltano.com) is set up with live chat powered by Intercom.

## Calendars

Shared calendars and meetings:
- [Community Meetings](https://calendar.google.com/calendar/u/1?cid=Y18wMWNqNDhoYTRoMTk5Y3RqZWZpODV0OWRnY0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t): Meetings open to the community
  - [Office Hours](/handbook/engineering/#office-hours): Weekly on Wednesday, owned by AJ
  - [Demo Day](/handbook/engineering/#demo-day): Weekly on Friday, owned by Taylor
- Team Meetings: Meetings with the team
  - Kick off: Weekly on Monday, owned by Douwe
- External Meetings: Meetings with users and partners
- Time Off: All-day events for periods team members will not be working

## Trademarks

Meltano is a registered trademark in the USA and China.

Further details, such as the registration number and certificate, can be found in the "Meltano" 1Password vault.
