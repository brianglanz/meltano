---
title: "Digital Marketing and Web"
sidebarDepth: 2
---

# Digital Marketing and Web
Given our limited resources we take an objective first approach to all our digital campaigns. We lead by setting objectives and what we need to achive and plan from there. What we put out we aim to be able to measure and refine over time. 

## Coming Soon
* updating the marketing site
* web page structure
* site maps

### SEO and Google analytics
#### SEO strategy and keywords

#### SEO Content templates 
* coming soon

### Web Design Kit
* include accessibility 

Channel and tactics: 

### GDPR and privacy

